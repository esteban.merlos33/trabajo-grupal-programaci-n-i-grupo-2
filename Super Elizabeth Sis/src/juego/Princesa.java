package juego;
import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Princesa {
	private double x;
	private double y;
	private int ancho;
	private int alto;
	private double velocidad;
	private Image imagen;
	private int vidas;
	private int puntaje;

	public Princesa(double x, double y, int ancho, int alto) {
		this.x = x;
		this.y = y;
		this.ancho = ancho;
		this.alto = alto;
		this.velocidad = 1.5;
		this.vidas = 3;
		this.imagen = Herramientas.cargarImagen("princesaderecha.png");
		this.puntaje = 0;

	}

	public int getPuntaje() {
		return this.puntaje;
	}

	public void sumaPuntaje() {
		puntaje += 5;
	}

	public int getVidas() {
		return this.vidas;
	}

	public double getX() {
		return this.x;
	}

	public double getY() {
		return this.y;
	}

	public double getAncho() {
		return this.ancho;
	}

	public double getAlto() {
		return this.alto;
	}

	public void dibujar(Entorno entorno) {
//		entorno.dibujarRectangulo(this.x, this.y, this.ancho, this.alto, 0, this.color);
		entorno.dibujarImagen(imagen, this.x, this.y, 0, 0.2);

	}

	public void moverHaciaAdelante(Entorno entorno) {
		x += velocidad;

	}

	public void moverHaciaAtras(Entorno entorno) {
		x -= velocidad;
	}

	public boolean chocasteConEntornoCero(Entorno entorno) {
		return x < 50;
	}

	public boolean chocasteConEntornoMitad(Entorno entorno) {
		return x > entorno.ancho() / 2;
	}

	public boolean chocasteconborde(Entorno entorno) {
		return x < 50 || x > entorno.ancho() / 2;
	}

	public void subir() {
		y -= 300;

	}

	public void bajar() {
		y -= -velocidad;
	}

	public boolean chocaConTecho(Entorno entorno) {
		return y < 250;
	}

	public boolean chocaConPiso(Entorno entorno) {

		return y >= 410;
	}

	public boolean tieneVidas() {
		if (vidas > 0) {
			return true;
		}
		return false;
	}

	public void pierdeVidas() {
		vidas--;

	}

	public void dispara(Fuego[] fuego) {
		for (int i = 0; i < fuego.length; i++) {
			if (fuego[i] == null) {
				fuego[i] = new Fuego(x, y, 20);
				return;

			}
		}
	}

	public void sumaVidas() {
		vidas++;
	}

	public void plusPuntaje() {
		puntaje += 10;
	}

	public boolean chocaConSoldado(Soldado s) {

		if ((this.x + this.ancho / 2 > s.getX() - s.getAncho() / 2
				&& this.x - this.ancho / 2 < s.getX() + s.getAncho() / 2
				|| this.x - this.ancho / 2 < s.getX() + s.getAncho() / 2
						&& this.x - this.ancho > s.getX() - s.getAncho() / 2)

				&& (this.y + this.alto / 2 > s.getY() - s.getAlto() / 2
						&& this.y + this.alto / 2 < s.getY() + s.getAlto() / 2)) {
			return true;
		}

		return false;

	}

	public boolean chocaConCorazon(Corazon c) {
		if ((this.x + this.ancho / 2 > c.getX() - c.getAncho() / 2
				&& this.x - this.ancho / 2 < c.getX() + c.getAncho() / 2)
				&& this.y + this.alto / 2 >= c.getY() - c.getAlto() / 2
				&& this.y - this.alto / 2 < c.getY() + c.getAlto() / 2) {
			return true;
		}
		return false;
	}

	public boolean chocaConRegalo(Regalo r) {
		if ((this.x + this.ancho / 2 > r.getX() - r.getAncho() / 2
				&& this.x - this.ancho / 2 < r.getX() + r.getAncho() / 2)
				&& this.y + this.alto / 2 >= r.getY() - r.getAlto() / 2
				&& this.y - this.alto / 2 < r.getY() + r.getAlto() / 2) {
			return true;
		}
		return false;
	}

	public boolean chocasteConObstaculo(Obstaculo o) {
		if ((this.x + this.ancho / 2 > o.getX() - o.getAncho() / 2
				&& this.x - this.ancho / 2 < o.getX() + o.getAncho() / 2)
				&& this.y + this.alto / 2 >= o.getY() - o.getAlto() / 2
				&& this.y - this.alto / 2 < o.getY() + o.getAlto() / 2) {
			return true;
		}
		return false;
	}

}
