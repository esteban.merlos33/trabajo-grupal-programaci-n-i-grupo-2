package juego;

import java.awt.Color;
import java.awt.Image;

import entorno.Herramientas;

public class Bola {
	private double x;
	private double y;
	private double diametro;
	private Color color;	
	private double velocidad;
	private Image imagen;
	public Bola(double x, double y, double diametro, double angulo, Color color) {
		this.x = x;
		this.y = y;
		this.color = color;
		this.velocidad=2;
		this.imagen=Herramientas.cargarImagen("princesa.png");
		}
	
	public double getX() {
		return this.x;
	}

	public double getY() {
		return this.y;
	}

	public double getDiametro() {
		return this.diametro;
	}
}
