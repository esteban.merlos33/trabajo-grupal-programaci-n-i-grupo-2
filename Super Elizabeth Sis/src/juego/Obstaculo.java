package juego;
import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Obstaculo {

	private double x;
	private double y;
	private int ancho;
	private int alto;
	private double velocidad;
	private Image img;

	public Obstaculo(double x, double y, int ancho, int alto) {
		this.x = x;
		this.y = y;
		this.ancho = ancho;
		this.alto = alto;
		this.velocidad = 0.8;
		this.img = Herramientas.cargarImagen("pinche.png");
	}

	public double getX() {
		return this.x;
	}

	public double getY() {
		return this.y;
	}

	public double getAncho() {
		return this.ancho;
	}

	public double getAlto() {
		return this.alto;
	}



	public void mover() {
		x -= this.velocidad;
	}

	public void dibujar(Entorno entorno) {
		entorno.dibujarImagen(img, this.x + 3, this.y, 0, 0.21);
	}

	public boolean chocasteConEntorno(Entorno entorno) { // Devuelve true si el lado derecho del obstaculo quedó fuera
															// del entorno
		return this.x + this.ancho / 2 < 0;
	}

	public void reaparecer(Obstaculo[] o, Entorno entorno) {// asigna un x especificico al objeto para evitar que se
															// superponga con otro del mismo tipo
		double Xmax = entorno.ancho();
		for (int i = 0; i < o.length; i++) {
			if (o[i] != null && o[i].getX() > Xmax) {
				Xmax = o[i].getX();
			}
		}
		this.x = Xmax + 200 + 200 * Math.random();
	}

}