package juego;
import java.awt.Image;
import entorno.Entorno;
import entorno.Herramientas;

public class Corazon {
	private double x;
	private double y;
	private int ancho;
	private int alto;
	private double velocidad;
	private Image imagen;

	public Corazon(double x, double y, int ancho, int alto) {
		super();
		this.x = x;
		this.y = y;
		this.ancho = ancho;
		this.alto = alto;
		this.velocidad = 1;
		this.imagen = Herramientas.cargarImagen("corazon-rojo.png");
	}

	public double getX() {
		return this.x;
	}

	public double getY() {
		return this.y;
	}

	public double getAncho() {
		return this.ancho;
	}

	public double getAlto() {
		return this.alto;
	}

	public void dibujar(Entorno entorno) {
		entorno.dibujarImagen(imagen, x, y, 0, 0.09);
	}

	public void mover() {
		x -= this.velocidad;
	}

	public boolean chocasteConEntorno(Entorno entorno) {
		return this.x + this.ancho / 2 < 0;

	}

	public boolean chocaConPrincesa(Princesa p) {
		if (this.x - this.ancho / 2 < p.getX() + p.getAncho() / 2
				&& p.getY() + p.getAlto() / 2 > this.y - this.alto / 2) {
			return true;
		}
		return false;
	}

	public void reaparecer(int i, Corazon[] c) {// asigna un x especificico al objeto para evitar que se superponga con
												// otro del mismo tipo
		if (i == 0) {
			this.x = c[1].getX() + 800 + 200 * Math.random();
		} else {
			this.x = c[0].getX() + 800 + 200 * Math.random();
		}
	}

}
