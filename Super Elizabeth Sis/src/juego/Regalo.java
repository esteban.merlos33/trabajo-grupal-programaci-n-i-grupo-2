package juego;
import java.awt.Image;
import entorno.Entorno;
import entorno.Herramientas;

public class Regalo {
	private double x;
	private double y;
	private int ancho;
	private int alto;
	private double velocidad;
	private Image imagen;

	public Regalo(double x, double y, int ancho, int alto) {

		this.x = x;
		this.y = y;
		this.ancho = ancho;
		this.alto = alto;
		this.velocidad = 1;
		this.imagen = Herramientas.cargarImagen("regalo.png");
	}

	public double getX() {
		return this.x;
	}

	public double getY() {
		return this.y;
	}

	public double getAncho() {
		return this.ancho;
	}

	public double getAlto() {
		return this.alto;
	}

	public void dibujar(Entorno entorno) {
		entorno.dibujarImagen(imagen, x, y, 0, 0.09);
	}

	public void mover() {
		x -= this.velocidad;
	}

	public boolean chocasteConEntorno(Entorno entorno) {
		return this.x + this.ancho / 2 < 0;

	}

	public boolean chocaConPrincesa(Princesa p) {
		if (this.x - this.ancho / 2 < p.getX() + p.getAncho() / 2
				&& p.getY() + p.getAlto() / 2 > this.y - this.alto / 2) {
			return true;
		}
		return false;
	}

	public void reaparecer(int i, Regalo[] r) {// asigna un x especificico al objeto para evitar que se superponga con
												// otro del mismo tipo
		if (i == 0) {
			this.x = r[1].getX() + 400 + 200 * Math.random();
		} else {
			this.x = r[0].getX() + 400 + 200 * Math.random();
		}
	}

}
