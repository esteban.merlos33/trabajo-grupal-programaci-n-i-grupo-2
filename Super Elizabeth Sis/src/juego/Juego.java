
package juego;
import java.awt.Color;
import java.awt.Image;

import javax.sound.sampled.Clip;

import entorno.Entorno;
import entorno.Herramientas;
import entorno.InterfaceJuego;

public class Juego extends InterfaceJuego {
	//Declaración de las variables
	private Entorno entorno;
	private Princesa princesa;
	private Fuego[] fuegos = new Fuego[3];
	private Soldado[] soldados = new Soldado[5];		
	private Obstaculo[] obstaculos = new Obstaculo[3];
	private Corazon[] corazones = new Corazon[2];
	private Regalo[] regalos = new Regalo[2];
	
	private Image fondo;
	private Image fondoGanador;
	private Image fondoPerdedor;
	private Image vidas;
	private Image puntaje;
	private Image soldadoImg;
	
	private int soldadosEliminados;
	private boolean terminado;
	private Clip musica;

	public Juego() {
		// Inicializa el objeto entorno y las variables declaradas
		entorno = new Entorno(this, "Pseudo Pong", 800, 600);
		fondo = Herramientas.cargarImagen("fondofinal.jpg");
		fondoGanador = Herramientas.cargarImagen("llegada.jpg");
		fondoPerdedor = Herramientas.cargarImagen("triste2.jpg");
		princesa = new Princesa(50, 410, 65, 90);

		obstaculos[0] = new Obstaculo(830, 425, 37, 60);
		obstaculos[1] = new Obstaculo(1130, 425, 37, 60);
		obstaculos[2] = new Obstaculo(1450, 425, 37, 60);

		fuegos[0] = null;
		fuegos[1] = null;
		fuegos[2] = null;

		soldados[0] = new Soldado(850, 410, 50, 100);
		soldados[1] = new Soldado(1350, 410, 50, 100);
		soldados[2] = new Soldado(1800, 410, 50, 100);
		soldados[3] = new Soldado(2300, 410, 50, 100);
		soldados[4] = new Soldado(2600, 410, 50, 100);
		soldadosEliminados = 0;

		corazones[0] = new Corazon(1300, 430, 65, 90);
		corazones[1] = new Corazon(2100, 430, 65, 90);

		regalos[0] = new Regalo(1000, 430, 65, 90);
		regalos[1] = new Regalo(2000, 430, 65, 90);

		vidas = Herramientas.cargarImagen("corazon-rojo.png");
		puntaje = Herramientas.cargarImagen("regalo.png");
		soldadoImg = Herramientas.cargarImagen("soldado.png");
		musica = Herramientas.cargarSonido("TOP.wav");
		terminado = false;

		this.entorno.iniciar();
		musica.start();
	}

//TICK
	public void tick() {
		if (!terminado) {

			// FONDO
			entorno.dibujarImagen(fondo, entorno.ancho() / 2, entorno.alto() / 2, 0, 1.1);

			// INDICADORES DE VIDAS Y PUNTAJE
			entorno.cambiarFont("Baskerville Old Face", 40, Color.black); // tipo de fuente del texto
			entorno.escribirTexto(String.valueOf(princesa.getPuntaje()), 700, 100); // texto de puntaje
			entorno.escribirTexto(String.valueOf(princesa.getVidas()), 80, 100); // texto de vidas
			entorno.escribirTexto(String.valueOf(soldadosEliminados), 580, 100); // texto del contador de soldados
			entorno.dibujarImagen(vidas, 52, 92, 0, 0.09); // imagen de vidas
			entorno.dibujarImagen(puntaje, 670, 85, 0, 0.09); // imagen de puntaje
			entorno.dibujarImagen(soldadoImg, 550, 90, 0, 0.1); // imagen del contador de soldados

			// PRINCESA
			princesa.dibujar(entorno);

			if (princesa.chocaConPiso(entorno) == false) {
				princesa.bajar();
			}
			if (princesa.chocaConPiso(entorno) && entorno.estaPresionada(entorno.TECLA_ARRIBA)) {
				princesa.subir();
			}

			if (!princesa.chocasteConEntornoMitad(entorno) && princesa.chocaConPiso(entorno)
					&& entorno.estaPresionada(entorno.TECLA_DERECHA)) {
				princesa.moverHaciaAdelante(entorno);
			}

			if (!princesa.chocasteConEntornoCero(entorno) && princesa.chocaConPiso(entorno)
					&& entorno.estaPresionada(entorno.TECLA_IZQUIERDA)) {
				princesa.moverHaciaAtras(entorno);
			}

			for (int i = 0; i < soldados.length; i++) {
				if (soldados[i] != null) {
					if (princesa.chocaConSoldado(soldados[i]) && princesa.tieneVidas()) {
						soldados[i] = null;
						princesa.pierdeVidas();
					}
				}
			}

			// SOLDADOS
			for (int i = 0; i < soldados.length; i++) {
				if (soldados[i] != null) {
					soldados[i].dibujar(entorno);
					soldados[i].mover();
				} else {
					soldados[i] = new Soldado(0, 410, 50, 100);
					soldados[i].reaparecer(soldados, entorno);
				}
			}

			for (int i = 0; i < soldados.length; i++) {
				if (soldados[i].chocasteConEntorno(entorno)) {
					soldados[i] = null;
				}
			}

			// OBSTACULOS
			for (int i = 0; i < obstaculos.length; i++) {
				if (obstaculos[i] != null && obstaculos[i].chocasteConEntorno(entorno)) {
					obstaculos[i] = null;
				}
			}

			for (int i = 0; i < obstaculos.length; i++) {
				if (obstaculos[i] != null) {
					obstaculos[i].dibujar(entorno);
					obstaculos[i].mover();
				} else {
					obstaculos[i] = new Obstaculo(0, 425, 37, 60);
					obstaculos[i].reaparecer(obstaculos, entorno);
				}
			}

			// FUEGO
			for (int i = 0; i < fuegos.length; i++) {
				if (fuegos[i] != null) {
					fuegos[i].mover();
					fuegos[i].dibujar(entorno);

					if (fuegos[i].chocasteconborde(entorno)) {
						fuegos[i] = null;
					}
				}
			}

			if (entorno.sePresiono(entorno.TECLA_ESPACIO)) {
				for (int i = 0; i < fuegos.length; i++) {
					if (fuegos[i] == null) {
						fuegos[i] = new Fuego(princesa.getX(), princesa.getY() - 23, 20);
						break;
					}
				}
			}

			// CHOQUE DE SOLDADOS Y FUEGO
			for (int i = 0; i < fuegos.length; i++) {
				for (int j = 0; j < soldados.length; j++) {
					if (fuegos[i] != null && soldados[j] != null && fuegos[i].chocasteConSoldado(soldados[j])) {
						fuegos[i] = null;
						soldados[j] = null;
						princesa.sumaPuntaje();
						soldadosEliminados += 1;
					}
				}
			}

			// CHOQUE DE PRINCESA Y OBSTACULOS
			for (int i = 0; i < obstaculos.length; i++) {
				if (obstaculos[i] != null && princesa.chocasteConObstaculo(obstaculos[i])) {
					obstaculos[i] = null;
					princesa.pierdeVidas();

				}
			}

			// CHOQUE DE FUEGO Y OBSTACULOS
			for (int i = 0; i < fuegos.length; i++) {
				for (int j = 0; j < obstaculos.length; j++) {
					if (fuegos[i] != null && obstaculos[j] != null && fuegos[i].chocasteConObstaculo(obstaculos[j])) {
						fuegos[i] = null;
					}
				}
			}

			// CORAZONES
			for (int i = 0; i < corazones.length; i++) {
				if (corazones[i] != null) {
					corazones[i].dibujar(entorno);
					corazones[i].mover();

					if (corazones[i].chocasteConEntorno(entorno) || princesa.chocaConCorazon(corazones[i])) {
						corazones[i].reaparecer(i, corazones);
						princesa.sumaVidas();
					}
				}
			}

			// REGALOS
			for (int i = 0; i < regalos.length; i++) {
				if (regalos[i] != null) {
					regalos[i].dibujar(entorno);
					regalos[i].mover();

					if (regalos[i].chocasteConEntorno(entorno) || princesa.chocaConRegalo(regalos[i])) {
						regalos[i].reaparecer(i, regalos);
						princesa.plusPuntaje();
					}
				}
			}

			// Terminó el juego
			if (princesa.getVidas() < 1 || soldadosEliminados == 10) {
				terminado = true;
				musica.stop();
			}

			// Veo si ganó o perdió
		} else {

			entorno.cambiarFont("Baskerville Old Face", 60, Color.black);
			if (princesa.getVidas() < 1) {
				entorno.dibujarImagen(fondoPerdedor, entorno.ancho() / 2, entorno.alto() / 2, 0, 1.1);
				entorno.escribirTexto("PERDISTE. FIN DEL JUEGO", entorno.ancho() / 27, entorno.alto() / 1.7);
				entorno.escribirTexto("Tu puntuación: " + princesa.getPuntaje(), entorno.ancho() / 7,
						entorno.alto() / 1.20);

			} else {
				entorno.cambiarFont("Baskerville Old Face", 60, Color.white);
				entorno.dibujarImagen(fondoGanador, entorno.ancho() / 2, entorno.alto() / 2, 0, 0.5);
				entorno.escribirTexto("¡GANASTE!", entorno.ancho() / 7, entorno.alto() / 1.9);
				entorno.escribirTexto("¡LLEGASTE AL CASTILLO! ", entorno.ancho() / 18, entorno.alto() / 1.5);
				entorno.escribirTexto("Tu puntuación: " + princesa.getPuntaje(), entorno.ancho() / 7,
						entorno.alto() / 1.15);
			}
		}
	}

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		Juego juego = new Juego();
	}

}
